/*
 * Created by Hider Sanchez 01/10/18 05:09 AM
 * Copyright (c) 2018. Intecs.pe All rights reserved.
 */

package pe.net.intecs.demodaggerandroid.ui.main;

import android.content.Context;
import android.util.Log;

import javax.inject.Inject;

public class MainPresenter implements MainPresenterContract {
   
   Context mContext;
   private String TAG=getClass().getSimpleName();
   
   @Inject
   public MainPresenter(Context context) {
      this.mContext=context;
   }
   
   @Override
   public void showView() {
      if(mContext == null) Log.i(TAG, "showView: context es null");
      else Log.i(TAG, "showView: context ok");
   }
}
