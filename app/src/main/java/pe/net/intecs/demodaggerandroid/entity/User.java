package pe.net.intecs.demodaggerandroid.entity;

public class User {
   public String firstname;
   public String lastname;
   
   public User(final String firstname, final String lastname) {
      this.firstname=firstname;
      this.lastname=lastname;
   }
   
   @Override
   public String toString() {
      return firstname + " " + lastname;
   }
}
