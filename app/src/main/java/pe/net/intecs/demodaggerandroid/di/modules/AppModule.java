/*
 * Created by Hider Sanchez 01/10/18 03:25 AM
 * Copyright (c) 2018. Intecs.pe All rights reserved.
 */

package pe.net.intecs.demodaggerandroid.di.modules;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {
   @Provides
   public Context provideAplicationContext(Application application) {
      return application.getApplicationContext();
   }
}
