/*
 * Created by Hider Sanchez 01/10/18 03:58 AM
 * Copyright (c) 2018. Intecs.pe All rights reserved.
 */

package pe.net.intecs.demodaggerandroid.ui.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;
import pe.net.intecs.demodaggerandroid.R;
import pe.net.intecs.demodaggerandroid.entity.User;
import pe.net.intecs.demodaggerandroid.ui.main.MainPresenter;

public class MainActivity extends AppCompatActivity {
   @Inject
   User          mUser;
   @Inject
   MainPresenter mMainPresenter;
   //@Inject
   //Context  mContext;
   
   @BindView(R.id.hello)
   TextView hello;
   
   @Override
   protected void onCreate(Bundle savedInstanceState) {
      
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_main);
      ButterKnife.bind(this);
      AndroidInjection.inject(this);
      hello.setText(mUser.toString());
      //if(mContext != null) Toast.makeText(this, "Contexto Injectado por dagger", Toast.LENGTH_LONG).show();
      
      mMainPresenter.showView();
   }
   
}
