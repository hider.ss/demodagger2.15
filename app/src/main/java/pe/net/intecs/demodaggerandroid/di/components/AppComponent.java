/*
 * Created by Hider Sanchez 01/10/18 03:29 AM
 * Copyright (c) 2018. Intecs.pe All rights reserved.
 */

package pe.net.intecs.demodaggerandroid.di.components;

import android.app.Application;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.support.AndroidSupportInjectionModule;
import pe.net.intecs.demodaggerandroid.App;
import pe.net.intecs.demodaggerandroid.di.modules.ActivityBuilderInject;
import pe.net.intecs.demodaggerandroid.di.modules.AppModule;

@Component(modules={AndroidInjectionModule.class, AndroidSupportInjectionModule.class, AppModule.class, ActivityBuilderInject.class})
public interface AppComponent {
   @Component.Builder
   interface Builder {
      @BindsInstance
      Builder aplication(Application aplication);
      
      AppComponent build();
   }
   
   void inject(App app);
}

